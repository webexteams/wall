import './App.css';
import { useEffect, useState } from 'react';
import axios from "axios";

import ImageGallery from './components/ImageGallery';
//import { BlockPicker } from 'react-color';

function App() {

  const [webexApp, setWebexApp] = useState(false);
  const [userId, setUserId] = useState();
  //const [email, setEmail] = useState();
  const [displayName, setDisplayname] = useState()
  const [copied, setCopied] = useState(false);
  const [spaceId, setSpaceId] = useState()
  const [spaceTitle, setSpaceTitle] = useState("Default Space Name")
  const [astronauts, setAstronauts] = useState(); // Space people!
  const baseUrl = "https://us-central1-webex-wall-500da.cloudfunctions.net/memberships?roomId=";

  const debuggingOn = false;
  const wallMakerBaseLink = "https://webex-wall.web.app/user?id=";
  const [wallMakerURL] = useState(wallMakerBaseLink+userId);
  const bPIIDataBug = true;


  const urlSearchParams = new URLSearchParams(window.location.search);
  const params = Object.fromEntries(urlSearchParams.entries());
  const bRunningInDev = !!params.dev;

  function dummyData() {
    console.warn("Demo data used");
    setUserId("b7284a20-6715-4af9-9d5c-e72afed276de");
    setDisplayname("Dorin Ciobanu");
    setSpaceId("706ca160-f43a-11eb-abcc-afd7a8d98163");
    setSpaceTitle("☀️ Hackathon Group ☀️");
  }  

  function updateGallery(spaceId) {
      console.log(baseUrl+spaceId);
      axios.get(baseUrl+spaceId).then( (response) => {
        console.log(response);
        console.log(response.data);
        setAstronauts(response.data);
      });
  }

  useEffect( () => {
    if (bRunningInDev) {
      dummyData();
      updateGallery(spaceId)
      return; //There's no spoon!
    }

    if(webexApp) {
      return;
    }
    const _webexApp = new window.Webex.Application();      
    _webexApp.onReady().then( () => {
      if(bPIIDataBug){
        dummyData();
      } else {
        _webexApp.context.getUser().then( (user) => { setUserId(user.id); setDisplayname(user.displayName) } )
        _webexApp.context.getSpace().then( (obj) => { setSpaceId(obj.id); setSpaceTitle(obj.title) })
      }
      setWebexApp(_webexApp);
    })
    .then(() => {
      return updateGallery(spaceId)
    })
    .catch((err)=>{
      console.error("Webex App Error", err)
    })

  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [webexApp, spaceId, bPIIDataBug, bRunningInDev])

  //Converting a useState object containing string into string so that it can be copied into clipboard for user.
  function objToString (obj) {
    let str = '';
    for (const [p, val] of Object.entries(obj)) {
        console.log(`${p}::${val}\n`)
        str += `${val}`;
        
    }
    return str;
}
  return (
    <div className="App">
      <div style={{border: "10px double whitesmoke", maxWidth: "max-content", padding: "2em 2em", borderRadius: "10px", backgroundColor: "#282C34"}}>
        <header className="App-header">
          

          <h1 style={{fontWeight: "bold", margin: "0 0 1em 0"}}>{spaceTitle}'s Social Wall 💯</h1>
          
          <div 
            onClick={() => {navigator.clipboard.writeText( objToString( {wallMakerURL} ) ); setCopied(true)}}
            style={{ 
                backgroundColor:"#e6fdf9",
                padding: "0.5em 1em",
                color: "black",
                borderRadius: "10px",
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
                cursor: "pointer",
                marginBottom: "2em"
            }}>
            {/* <button onClick={ () => {navigator.clipboard.writeText( objToString( {wallMakerURL} ) )}}
                    style={{
                          backGroundColor:"lightblue",
                          color:"black",
                    }}
            >copy</button> */}
            {copied ? <>Copied! 🥳</> : 
              <><img src="./copy.png" height="100px" width="100px" style={{borderRadius: "5px", cursor: "pointer"}}  alt=""/>
              <div style={{fontSize: "1.7rem"}}>You can copy the link to create your own wall to share!<br /> <span style={{fontSize: "16px"}}>{wallMakerBaseLink+userId}</span> </div> </>
            } 
          </div>
          {/* <p><br /> This is a place for you to get to know the people you share space with. You can copy the link below and open a web
          browser tab and copy this link in to create your own wall to share.</p> */}
          
          
        
          {debuggingOn 
          ? 
            <div>
            <p>Webex Object Information</p>
            <p>Webex: {JSON.stringify({webexApp})} </p>
            <p>API DATA</p>
            <p>Cloud Function API Data {JSON.stringify({spaceAPI: astronauts})} </p>
            <p>Space ID: {JSON.stringify({spaceId})} </p>
            <p>Space Title: {JSON.stringify({spaceTitle})} </p>
            <p>User id: {userId}</p>
            <p>Display Name: {displayName}</p>
            </div>
          : <p></p> }


          <h2 style={{color: "rgb(255, 255, 255, 0.7)"}}>Meet the team 😎 </h2>
          <ImageGallery astronauts={astronauts}/>
        </header>
        
        
      </div>
    </div>
  );
}

export default App;
