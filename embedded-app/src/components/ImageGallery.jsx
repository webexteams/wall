import {Card, Row } from 'react-bootstrap';
import './ImageGallery.css';

function ImageGallery(props) {
    if (!!props.astronauts) {
        return props.astronauts.map((astronaut)=>{
            return (
                <Card className="card" style={{ width: '30rem', margin: "1em" }}>
                    <Card.Img variant="top" src={"https://firebasestorage.googleapis.com/v0/b/webex-wall-500da.appspot.com/o/images%2F"+astronaut.id+"?alt=media"} 
                        onError={(e)=>{e.target.onerror = null; e.target.src="missing_pic.jpg"}}/>
                    <Card.Body>
                    <Card.Title className="title">{astronaut.name}</Card.Title>
                    </Card.Body>
                </Card>
            )
        });
    } else {
        return <Row></Row>
    }
}


export default ImageGallery;