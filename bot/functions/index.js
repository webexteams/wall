const functions = require("firebase-functions");
const axios = require("axios");

exports.memberships = functions.https.onRequest((request, response) => {
  const roomId = request.query.roomId;
  functions.logger.info(roomId, {structuredData: true});
  axios.defaults.baseURL = "https://webexapis.com/v1";
  axios.defaults.headers.common["Authorization"] = `Bearer ${functions.config().bot.token}`;
  response.set("Access-Control-Allow-Origin", "*");

  axios.get("/memberships?roomId=" + roomId)
      .then( (reqRes) => {
        functions.logger.info("Memberships returned successfully", {structuredData: true});

        const people = [];

        reqRes.data.items.forEach((person) => {
          const decodedId = Buffer.from(person.personId, "base64").toString("utf-8");
          const userId = decodedId.substring(decodedId.lastIndexOf("/") + 1);

          people.push({
            "id": userId,
            "email": person.personEmail,
            "name": person.personDisplayName,
          });
        });

        response.status(200).json(people);
      })
      .catch( (details) => {
        functions.logger.error("Memberships returned an error", console.err, {structuredData: true});
        response.status(500).json({
          message: "Failure",
          code: details.response.status,
        });
      });
});
