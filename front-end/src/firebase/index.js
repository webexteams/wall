import firebase from "firebase/compat/app";
import "firebase/storage"
import { getStorage } from "firebase/storage";

const firebaseConfig = {
    apiKey: "AIzaSyBRj0oCaQxKMQ_zL2Ww3xQdR1bsLjfuFIM",
    authDomain: "webex-wall-500da.firebaseapp.com",
    projectId: "webex-wall-500da",
    storageBucket: "webex-wall-500da.appspot.com",
    messagingSenderId: "5965321882",
    appId: "1:5965321882:web:7bf105fb1c586305deb123",
    measurementId: "G-MXSS9C7Q2R"
  };

var app = firebase.initializeApp(firebaseConfig);

const storage = getStorage(app);

export { storage, firebase as default}