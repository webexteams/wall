import { useState } from 'react';
import './App.css';
import Dropzone from './components/Dropzone'
import { BlockPicker } from 'react-color';
import { storage } from "./firebase";
import { uploadBytes, ref } from '@firebase/storage';
import { toPng } from "html-to-image"
import { useLocation } from 'react-router-dom';

function App() {
  let query = useQuery();
  const [images, setImages] = useState([]);
  const [selected, setSelected] = useState(null);
  // eslint-disable-next-line no-unused-vars
  const [collageImages, setCollageImages] = useState([{id: "1", source: "https://www.macmillandictionary.com/us/external/slideshow/full/Grey_full.png"}, {id: "2", source: "https://www.macmillandictionary.com/us/external/slideshow/full/Grey_full.png"}, {id: "3", source: "https://www.macmillandictionary.com/us/external/slideshow/full/Grey_full.png"}, {id: "4", source: "https://www.macmillandictionary.com/us/external/slideshow/full/Grey_full.png"}, {id: "5", source: "https://www.macmillandictionary.com/us/external/slideshow/full/Grey_full.png"}, {id: "6", source: "https://www.macmillandictionary.com/us/external/slideshow/full/Grey_full.png"}])
  const [borderColor, setBorderColor] = useState("#d9e3f0");

  const handleSwitch = (id, index) => {
    if (selected) {
      for (let i = 0; i < collageImages.length; i++) {
        if (collageImages[i].id === id && index === i) {
          collageImages[i] = selected;
        }
      }
      setSelected(null)
    }
  }

  const handleChangeComplete = (color) => {
    setBorderColor(color.hex);
  };

  const handleScreenshot = () => {
    var node = document.getElementById('my-collage');

    toPng(node)
      .then(async (base64Data) => {
        function b64toBlob(dataURI) {
    
          var byteString = atob(dataURI.split(',')[1]);
          var ab = new ArrayBuffer(byteString.length);
          var ia = new Uint8Array(ab);
          
          for (var i = 0; i < byteString.length; i++) {
              ia[i] = byteString.charCodeAt(i);
          }
          return new Blob([ab], { type: 'image/png' });
        }

        handleUpload(b64toBlob(base64Data));
      })
  }

  const handleUpload = (file) => {
    const storageRef = ref(storage, `images/${query.get("id")}`);

    // 'file' comes from the Blob or File API
    uploadBytes(storageRef, file).then((snapshot) => {
        alert("upload successful! :D")
    }).catch((err) => alert("upload failed :("));
  }

  const handleDelete = () => {
    if (selected) {
      let editArr = images;
      for (let i = 0; i < editArr.length; i++) {
        if (editArr[i].id === selected.id) {
          editArr.splice(i, 1);
        }
      }
      setSelected(null);
      setImages(editArr);
    }
  }

  return (
    <div style={{display: "flex", alignItems: "center", justifyContent: "center", flexDirection: "column", backgroundColor: "#282c34", color: "white", minHeight: "100vh", height: "100%"}}>

      <h1>Social Wall Collage Maker</h1>
      
      <div style={{display: "flex", margin: "2em 0"}}>
        <div id="my-collage" className="container" style={{backgroundColor: borderColor}}> 
          {collageImages.map((img, i) => { 
            if (i > 5) return null; 
            return <img onClick={() => handleSwitch(img.id, i)} className="image" src={img.source} style={{objectFit: "cover"}} alt=""/>
          })}
        </div>

        <div style={{marginLeft: "1em", display: "flex", justifyContent: "space-between", flexDirection: "column"}}>
          <img src="./webex.jpg" height="auto" width="170px" style={{borderRadius: "5px"}}  alt=""/>
          <BlockPicker triangle="hide" color={borderColor} onChangeComplete={handleChangeComplete}/>
        </div>
      </div>
      <div style={{margin: "2em 0"}}>
        {images.map((img) => <img onClick={() => setSelected(img)} height="150px" width="150px" src={img.source} style={{cursor: "pointer", borderRadius: "10px", marginRight: "8px", objectFit: "cover", border: `${img.id === selected?.id ? "#5DED5C 3px solid" : "rgb(0,0,0,0) 2px solid"}`}}  alt=""/>)}
      </div>
      <Dropzone images={images} setImages={setImages} />
        
  
      <div style={{display: "flex", marginTop: "2em", alignItems: "center"}}>
        <img onClick={handleScreenshot} src="./upload.png" height="100px" width="100px" style={{borderRadius: "5px", cursor: "pointer"}}  alt=""/>
        {selected && <img onClick={handleDelete} src="./trash.png" height="70px" width="70px" style={{borderRadius: "5px", cursor: "pointer", marginLeft: "2em"}}  alt=""/>}
      </div>
      
      
    </div>
  );
}

function useQuery() {
  return new URLSearchParams(useLocation().search);
}

export default App;
