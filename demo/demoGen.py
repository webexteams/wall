import os
import sys
import shutil
import subprocess

MACHINE="Brian"
HUMAN="Matthew"

script = [ 
    [MACHINE,     1, "Hello human! I've noticed you've been kinda lonely lately. No matter how many cats you're letting in, it's not getting better!"],
    [MACHINE,     1, "You know, cats can really push my buttons! Why don't you get to know your co-workers better instead?"],
    [HUMAN,       1, "But why, mister synthesized voice person?!"],
    [MACHINE,     1, "I'm glad you asked! By getting to know the people you work with, you'll build new connections, feel empathy and leave me alone every once in a while so I can cool off my CPU!"],
    [HUMAN,       1, "How would I do that? All I see is names on a cold, cold screen!"],
    [MACHINE,     1, "I have the right tool for you pretend human! It's called Social Wall. It's an app/tab inside Webex. It'll show you what people want you to know about them. Things like their interests and pictures of their family. It's like a virtual cubical."],
    [HUMAN,       0, "Great! Can you show me how it works?"],
    [MACHINE,     60, "Of course! Start the Webex client first. Then add the Social Wall app in the space with your team-mates. Easy!"],
    [HUMAN,       10, "Beautiful!"],
    [HUMAN,        2, "How do I change my story?"],
    [MACHINE,     60, "Also easy. Open the link you see inside the tab and add your photos. When ready, press the Upload button."],
    [MACHINE,      2, "See how easy it was! Sometimes a picture says 1024 words!"]
]

if os.path.exists('output'):
    if os.path.exists('output/demoPlay.py'):
        shutil.rmtree('output')

os.mkdir("output")

demoPlay = open("output/demoPlay.py", "w")
demoPlay.write("""from pygame import mixer # Load the required library
import sys
import time

mixer.init()

def doPlay(fname, delay):
	mixer.music.load(fname)
	mixer.music.play()
	while mixer.music.get_busy():
		time.sleep(1)
	time.sleep(delay)

""")
    

for i in range(0, len(script)):
    subprocess.call('aws --region us-east-1 polly synthesize-speech --engine neural --text-type text --output-format "mp3" --voice-id "' + script[i][0] + '" --text "' + script[i][2] + '" output/' + str(i) + '.mp3"', shell=True)
    demoPlay.write('doPlay("' + str(i) + '.mp3", ' + str(script[i][1]) + ')\n')