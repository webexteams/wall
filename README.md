# SOCIAL WALL #

## Description
Add a personal touch to webex spaces. Tell people something personal about yourself and facilitate connections. We’re one team!
The Wall is an application that brings the equivalent of cubicle decorating to the virtual world. It allows members of a space to see your information on a digital wall. The users can see what’s important for others and discover things they have in common!

* Memberships API: [https://us-central1-webex-wall-500da.cloudfunctions.net/memberships?roomId=706ca160-f43a-11eb-abcc-afd7a8d98163](https://us-central1-webex-wall-500da.cloudfunctions.net/memberships?roomId=706ca160-f43a-11eb-abcc-afd7a8d98163)

* CI Pipeline: [https://bitbucket.org/webexteams/wall/addon/pipelines/home#!/](https://bitbucket.org/webexteams/wall/addon/pipelines/home#!/)

* Stand-alone website: [https://webex-wall.web.app/user?id=b7284a20-6715-4af9-9d5c-e72afed278de](https://webex-wall.web.app/user?id=b7284a20-6715-4af9-9d5c-e72agad276de)
* App: [https://webex-wall-500da.web.app/?dev=true](https://webex-wall-500da.web.app/?dev=true)


## Limitations
Since the participants list isn’t available through the embedded app framework, the users have to add a service bot into the space.
The app isn’t properly secured

Some of the API has errors so we added a hardcoded boolean check before accessing certain api calls, and instead gave the embedded app Dummy data.
## Architecture
* The app is heavily reliant on Firebase services:
* Realtime Database for the user content (except files)
* Cloud Storage (for the composite image file)
* Hosting (for hosting the app and the stand-alone site)
Cloud Functions
## Bot Component
* MVP: The bot doesn’t react to any of the user input. It’s in the space for the sole purpose of being able to call /memberships API. The /memberships call will be proxied using a Cloud Function.

* Stretch goal: The bot reacts to being added to the space by writing instructions in the space about how to add your information and the Embedded App


## Stand-alone site
* The site allows a user to edit their own information that will go on the wall. The site takes as a parameter the user’s UUID to be used in the Store function at a later time (see bellow)

* The site presents the user with a visual editor that allows compositing multiple images uploaded from the local drive.

* When the user is ready to save their information, the site creates the composite image and uploads it to Firebase Cloud Storage using the user ID’s as the key/file name.

* The standalone site receives the user id through the website's url.

## Embedded App
A single page app that:
* Uses the Embedded Apps SDK to obtain the space ID

* Queries the API that returns the space memberships list (see Bot Component)

* If the query fails, it provides instructions for adding the bot (stretch goal)

* If the query succeeds, it displays a gallery from the composite images provided by the users (see Stand-alone site)

* If a user does not have a composite image, display an empty box with their name on it (MVP) and send a private message from the bot to each of those users, containing the link to their stand-alone site page (stretch goal)
