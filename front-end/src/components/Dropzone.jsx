import React, {useCallback} from 'react';
import {useDropzone} from 'react-dropzone';
import uuid from 'react-uuid';

export default function Dropzone({images, setImages}) {
    const onDrop = useCallback(acceptedFiles => {
        const newFiles = [];
        acceptedFiles.forEach(file => {
            file = {
                ...file,
                source: URL.createObjectURL(file),
                id: Date.now() + uuid()
            };
            newFiles.push(file);
        })
       
        console.log(images);
        setImages([...images, ...newFiles]);
    }, [images, setImages])
    const {getRootProps, getInputProps, isDragActive} = useDropzone({onDrop, accept: 'image/jpeg, image/png'})

    return (
        <div {...getRootProps()} style={{fontWeight: "bold", letterSpacing: 1.3, lineHeight: 1.7, border: "2px dashed gray", cursor: "pointer", backgroundColor: "#f9f9f9", padding: "2em", width: 500, height: 40, color: "#282c34", display: "flex", justifyContent: "center", alignItems: "center"}}>
            <input {...getInputProps()} />
            {isDragActive ? <>Drop them here!</> : <div style={{textAlign: "center"}}>Drag 'n' drop some files here, or click to select files
            <br /><em>(Only *.jpeg and *.png images will be accepted)</em></div>}
        </div>
    )
}